import React from "react"
import PropTypes, { object } from "prop-types"

import NewsImg from "gatsby-image"
import GridLayout from "../gridLayout/GridLayout"

import getCategoryClass from "../../utils/getCategoryClass"

import commonStyles from "../../styles/common.module.css"
import newsStyles from "./_newsevents.module.css"

const NewsEvents = props => {
  const { newsEvents, mainLimit } = props

  newsEvents.forEach(data=>{
    data.date = data.blogDate.substring(0,2)
    data.month = monthArr[parseInt(data.blogDate.substring(3,5))-1]
    data.year = data.blogDate.substring(6,10)
  })

  const mainNewsEvents = newsEvents.slice(0,mainLimit)
  const sideNewsEvents = newsEvents.slice(mainLimit,mainLimit+4)

  return (
    <>
      <div className={`${commonStyles.container} ${commonStyles.margin_t_100}`} id="blog">
        <div className={newsStyles.heading}>
          <div className={newsStyles.title_wrapper}>
            <div className={newsStyles.title}>
              <p className={newsStyles.headerPara}>#News #Events #Blog</p>
              <h2>
                <strong>News & Events</strong>
              </h2>
            </div>
            <div className={newsStyles.subTitle}>
              <p>
                Discover how to anticipate and adapt to the latest trends and
                digital
              </p>
            </div>
          </div>

          <div className={newsStyles.newsbtn}>
            <button className={newsStyles.button} type="button">
              BROWSE ALL
            </button>
          </div>
        </div>

        <GridLayout>
          <div className="layout_lg_8">
            <GridLayout>
              {mainNewsEvents.map((content, index) => (
                <div className="layout_lg_6" key={index}>
                  <NewsEvent
                    content={content}
                    index={index}
                  />
                </div>
              ))}
            </GridLayout>
          </div>
          <div className="layout_lg_4">
            {sideNewsEvents
              ? sideNewsEvents.map((list, index) => (
                  <div className={newsStyles.totalDiv} key={index}>
                    <div className={newsStyles.head1} key={index}>
                      <div>
                        <h4 className={newsStyles.month1}>
                          <strong>{list.month}</strong>
                        </h4>
                        <h2 className={newsStyles.date1}>
                          <strong>{list.date}</strong>
                        </h2>
                      </div>
                      <div className={newsStyles.sidebarline}></div>
                      <div className={newsStyles.button4}>
                        <div className={getCategoryClass(list.category)} />
                        <h4 className={newsStyles.sidebarTitle}>
                          <strong>{list.title}</strong>
                        </h4>
                      </div>
                    </div>
                  </div>
                ))
              : null}
          </div>
        </GridLayout>
      </div>
    </>
  )
}

NewsEvents.propTypes = {
  newsEvents: PropTypes.arrayOf(object),
  mainLimit:PropTypes.number
}
NewsEvents.defaultProps = {
  mainLimit: 2
}
export default NewsEvents;

const NewsEvent = ({content}) => {

  const { image, date, month, year, title, description, category } = content

  return (
    <div className={commonStyles.container}>
      <div>
        <NewsImg fluid={image} className={newsStyles.img} />
      </div>
      <div className={newsStyles.para3}>
        <div className={newsStyles.button1}>
          <div className={getCategoryClass(category)} />
        </div>
        <p className={newsStyles.date}>{`${date}, ${month} ${year}`}</p>
      </div>
      <h3 className={newsStyles.contentTitle}>{title}</h3>
      <p className={newsStyles.para4}>{description}</p>
      <div className={newsStyles.para5}>
        <div className={newsStyles.button3}>
          <button type="button" className={newsStyles.button2}>
            +
          </button>
        </div>
        <p className={newsStyles.para6}>READ MORE</p>
      </div>
    </div>
  )
}

const monthArr = [
  "Jan",
  "Feb",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "Sept",
  "Oct",
  "Nov",
  "Dec"
]