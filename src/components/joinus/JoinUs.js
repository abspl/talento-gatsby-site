import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Partnerimg from "gatsby-image"

import GridLayout from "../gridLayout/GridLayout"

import commonStyles from "../../styles/common.module.css"
import partnerStyles from "./_joinUs.module.css"

const JoinUs = (props) => {

  const { onInstructor, onPartner } = props

  const data = useStaticQuery(graphql`
    query {
      instructorimg: file(
        relativePath: { eq: "partner_images/instructorimg1.png" }
      ) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
      partnerimg: file(relativePath: { eq: "partner_images/partnerimg1.png" }) {
        childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  return (
    <div className={commonStyles.overflow_hidden}>
      <section className={partnerStyles.partner_bg}>
        <GridLayout
          applyContainerClassName={false}
          containerStyles={{ padding: "0 20px" }}
        >
          <div className={`${partnerStyles.col1} layout_lg_6`}>
            <GridLayout applyContainerClassName={false}>
              <div className={`${partnerStyles.partCol} layout_md_6`}>
                <Partnerimg
                  className={partnerStyles.instImg}
                  fluid={data.instructorimg.childImageSharp.fluid}
                  alt="instructor img"
                />
              </div>
              <div className="layout_md_6">
                <h2 className={partnerStyles.heading}>
                  <strong>
                    Become an <br />
                    Instructor
                  </strong>
                </h2>
                <p className={partnerStyles.para}>
                  Lorem ipsum dolor sit amet, consectetur adipising elit, sed
                  toeiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam
                </p>
                <button type="button" className={partnerStyles.btn} onClick={onInstructor}>
                  APPLY NOW
                </button>
              </div>
            </GridLayout>
          </div>

          <div className={`${partnerStyles.col2} layout_lg_6`}>
            <GridLayout applyContainerClassName={false}>
              <div className={`${partnerStyles.partCol} layout_md_6`}>
                <Partnerimg
                  className={partnerStyles.partImg}
                  fluid={data.partnerimg.childImageSharp.fluid}
                  alt="partner img"
                />
              </div>
              <div className="layout_md_6">
                <h2 className={partnerStyles.heading}>
                  <strong>
                    Become a <br />
                    Partner
                  </strong>
                </h2>
                <p className={partnerStyles.para}>
                  Lorem ipsum dolor sit amet, consectetur adipising elit, sed
                  toeiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam
                </p>
                <button type="button" className={partnerStyles.btn1} onClick={onPartner}>
                  APPLY NOW
                </button>
              </div>
            </GridLayout>
          </div>
        </GridLayout>
      </section>
    </div>
  )
}

JoinUs.propTypes = {
  onInstructor: PropTypes.func,
  onPartner: PropTypes.func
}

export default JoinUs;