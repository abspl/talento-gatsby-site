import React from "react"

import commonStyles from "../../styles/common.module.css"
import partnerStyles from "./_partners.module.css"
import GridLayout from "../gridLayout/GridLayout"

export default () => {
  return (
    <section>
      <div className={commonStyles.container}>
        <div className={partnerStyles.title}>
          <h4>#Partners</h4>
          <h2>
            <strong>Who have we worked with?</strong>
          </h2>
        </div>
        <div className={partnerStyles.section}>
          <GridLayout
            layoutStyles={{ justifyContent: "center", textAlign: "center" }}
            applyContainerClassName={false}
          >
            <div className="layout_md_2">1</div>
            <div className="layout_md_2">2</div>
            <div className="layout_md_2">3</div>
            <div className="layout_md_2">4</div>
            <div className="layout_md_2">5</div>
          </GridLayout>
        </div>
      </div>
    </section>
  )
}
