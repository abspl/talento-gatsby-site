module.exports = {
  siteMetadata: {
    title: `Talento Academy`,
    description: `Talento Academy is rated as the one of the best IT Education & Training Institute in Tirunelveli, we the group of individuals devoted to excellence in teaching. We think disruptively to deliver tailor-made courses for student and professionals, all while hoping to make some positive impact in the educational community. We teach the magic of coding by hands-on and by the best practices followed in the software industry.`,
    author: `@gatsbyjs`,
    headerListOptions: [
      { name: "Home" },
      { name: "About" },
      {
        name: "Course", innerMenu: [
          { name: "Oracle certifed java course" },
          { name: "Full Stack Developer" },
          { name: "Proficent Programmer" },
          { name: "Object Oriented App Developer" }
        ]
      },
      { name: "Blog" },
      { name: "Contact" }
    ],
    courses: [
      "C",
      "C++",
      "Python",
      "Java",
      "JavaScript",
      "Andriod",
      "Ios",
      "React",
      "Angular",
      "PHP"
    ]
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `talento-academy`,
        short_name: `talento`,
        start_url: `/`,
        background_color: `#071657`,
        theme_color: `#071657`,
        display: `minimal-ui`,
        icon: `src/images/logo.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
  ],
}
